# lastresort-api

> This API was created to control booking actions such as: search booking availability, book reservations 
> or cancel a booking. 
> <br>As this hotel is the last available place to have a stay at Cancun, it has a huge demand of access
> to try to book a reservation. Therefore, the services systems are formed by 3 API's, aiming to reduce downtime by making the services independent.
> <br>There is a Eureka Server, which register all the API's there are going to communicate in between themselves. There is a second
> API which the client can access and demands its booking services, and finally, there a third API where all the booking services are applied.
> <br>The idea is to scale the application by creating another APIs for different kinds of services without having to change much the running ones and granting a 
> minimum downtime.

## References

APIs which depend from the services of this one are related 
at the [Stakeholders](https://gitlab.com/TiagoSeixas/lastresort-cancun/-/wikis/Stackholders) session.

## Documentation

### Diagrams

* Entity and relationship model

![last_resort_class_diagram](Diagrams/last_resort_class_diagram.png "Entity and relationship model")

* Search the availability dates

![get-available-days](Diagrams/get-available-days.png "get-available-days")

* Book dates

![book-dates](Diagrams/book-dates.png "book-dates")

* Get booking

![get-bookings](Diagrams/get-bookings.png "get-bookings")

* Modify a booking

![modify-booking](Diagrams/modify-booking.png "modify-booking")

* Cancel a booking

![cancel-booking](Diagrams/cancel-booking.png "cancel-booking")

___
### Error handling

Errors in the api are represented by the json:

```json
{
    "httpStatus": integer,
    "message" : "string"
}
```


### JOBS
#### - Availability DataLoad
There is a job to verifies the range of availability days (configured on a property file)
and includes the days of availability (based on the range). It executes as soon as the API runs and 
every at midnight.

The initial dataload also includes an `USER` for testing:
```json
{
    "email" : "hotelmanager@gmail.com",
    "password" : "hotelmanager"
}
```

### SERVICES

#### - Sign in to the API

_Service responsible to log in the user inside the API. Uses JWT._

* Url: `POST http://localhost:8090/api/auth/signin`
```json
{
    "email" : "string",
    "password" : "string"
}
```

#### - Register a new user to the API.

_Service responsible to register a new user to the API.<br>
Attention: will be given only regular user rules._

* Url: `POST http://localhost:8090/api/auth/register`
```json
{
    "username" : "string",
    "email" : "string",
    "password" : "string"
}
```

#### - Search available dates

_Service to search the available dates to be booked. It brings an available range of 30 days that can be set on the API property file._ <br>
_Needs to be logged in (send the authorization header: token generated at login endpoint)._

* Url: `GET http://localhost:8090/api/availability/get-available-days`
```json
{
    "items": [{
            "id": number,
            "day": date, //yyyy-mm-dd
            "booked": boolean
        }],
    "size": number
}
```

#### - Booking dates

_Service to book available dates._<br>
_Needs to be logged in (send the authorization header: token generated at login endpoint)._

* Url: `POST http://localhost:8090/api/availability/book-dates/{userId}`


#### - Get Bookings

_Service to list all the bookings._<br>
_Needs to be logged in (send the authorization header: token generated at login endpoint)._

* Url: `GET http://localhost:8090/api/availability/get-bookings`
```json
[{
    "id": number,
    "checkin": date, //yyyy-mm-dd
    "checkout": date, //yyyy-mm-dd
    "cancelled": boolean,
    "pax": number
}]
```


#### - Modify Booking

_Service to modify an existing reservation._<br>
_Needs to be logged in (send the authorization header: token generated at login endpoint)._

* Url: `PUT http://localhost:8090/api/availability/modify-booking/{bookingId}}`
```json
{
    "checkin": date, //yyyy-mm-dd
    "checkout": date, //yyyy-mm-dd
    "pax": {
        "id": number
    }
}
```


#### - Cancel booking

_Service to cancel a booking by informing the booking id._<br>
_Needs to be logged in (send the authorization header: token generated at login endpoint)._

* Url: `DELETE http://localhost:8090/api/availability/cancel-booking/{bookingId}`



## Execution

### Database configuration

1. Execute PostgreSQL database running on a default port.

* Execute following script:
````sql
CREATE ROLE LASTRESORT LOGIN SUPERUSER PASSWORD 'password';
CREATE DATABASE LASTRESORT;
GRANT ALL PRIVILEGES ON DATABASE LASTRESORT TO LASTRESORT;
````
_Once the API's above are executed, all the databased used by its services will be generated automatically._

2. Run the [lastresort-discover-api](https://gitlab.com/TiagoSeixas/lastresort-discover-api)

_This API is a Eureka Server to register different feign clients._
```sh
$ mvn clean package spring-boot:run
```

3. Run the [lastresort-api](https://gitlab.com/TiagoSeixas/lastresort-cancun)

_This API is a gateway used to access the booking services._
```sh
$ mvn clean package spring-boot:run
```

4. Run the [lastresort-availability-api](https://gitlab.com/TiagoSeixas/lastresort-availibility-api)

_This API holds all the booking services._
```sh
$ mvn clean package spring-boot:run
```

5. [Optional] [Postman](https://www.postman.com/downloads/ "postman") can be used to test the API with the collection. It needs only to import this file: [lastresort.json](Diagrams/lastresort.postman_collection.json "lastresort.json").
