package com.cancun.lastresort;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.context.ApplicationPidFileWriter;
import org.springframework.cloud.openfeign.EnableFeignClients;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;

@EnableJpaAuditing
@EnableFeignClients
@SpringBootApplication
public class LastresortApplication {

    public static void main(final String[] args) {
        final SpringApplication lastResortApplication = new SpringApplication(LastresortApplication.class);
        lastResortApplication.addListeners(new ApplicationPidFileWriter()); /* Register PID */

        lastResortApplication.run(args);
    }
}
