package com.cancun.lastresort.configuration.security;

import static com.cancun.lastresort.domain.enumeration.PrivilegeEnum.PRIVILEGE_ADMIN_READ;
import static com.cancun.lastresort.domain.enumeration.PrivilegeEnum.PRIVILEGE_HOTEL_ADMIN_READ;
import static com.cancun.lastresort.domain.enumeration.PrivilegeEnum.PRIVILEGE_USER_READ;
import static java.util.Collections.singletonList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.dao.DaoAuthenticationProvider;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.method.configuration.EnableGlobalMethodSecurity;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;
import org.springframework.security.config.http.SessionCreationPolicy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.web.authentication.UsernamePasswordAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;

import com.cancun.lastresort.domain.enumeration.PrivilegeEnum;

import lombok.RequiredArgsConstructor;

@Configuration
@EnableWebSecurity
@RequiredArgsConstructor
@EnableGlobalMethodSecurity(prePostEnabled = true)
public class SecurityConfiguration extends WebSecurityConfigurerAdapter {

    private final UserPrincipalService userPrincipalService;
    private final JwtRequestFilter jwtRequestFilter;
    private final JwtAuthenticationEntryPoint jwtAuthenticationEntryPoint;

    @Override
    protected void configure(final AuthenticationManagerBuilder auth) {
        auth.authenticationProvider(authenticationProvider());
    }

    @Override
    protected void configure(final HttpSecurity security) throws Exception {
        security
            .csrf().disable().cors()
            .and()
            .authorizeRequests().antMatchers("/auth/**").permitAll()
            .antMatchers(HttpMethod.GET, "/availability/**")
            .hasAnyAuthority(PRIVILEGE_USER_READ.name(), PRIVILEGE_ADMIN_READ.name(), PRIVILEGE_HOTEL_ADMIN_READ.name())
            .antMatchers(HttpMethod.POST, "/availability/**")
            .hasAnyAuthority(PRIVILEGE_USER_READ.name(), PRIVILEGE_ADMIN_READ.name(), PRIVILEGE_HOTEL_ADMIN_READ.name())
            .anyRequest().authenticated()
            .and()
            .logout()
            .and()
            .exceptionHandling().authenticationEntryPoint(jwtAuthenticationEntryPoint)
            .and()
            .sessionManagement().sessionCreationPolicy(SessionCreationPolicy.NEVER);

        /**
         * Add a filter to validate the tokens with every request
         **/
        security.addFilterBefore(jwtRequestFilter, UsernamePasswordAuthenticationFilter.class);
    }

    @Bean
    public AuthenticationManager customAuthenticationManager() throws Exception {
        return authenticationManager();
    }

    @Bean
    protected DaoAuthenticationProvider authenticationProvider() {
        final DaoAuthenticationProvider provider = new DaoAuthenticationProvider();
        provider.setUserDetailsService(userPrincipalService);
        provider.setPasswordEncoder(encoder());
        return provider;
    }

    @Bean
    public CorsConfigurationSource corsConfigurationSource() {
        final UrlBasedCorsConfigurationSource config = new UrlBasedCorsConfigurationSource();
        final CorsConfiguration cors = new CorsConfiguration().applyPermitDefaultValues();
        cors.setAllowedMethods(singletonList("*"));
        config.registerCorsConfiguration("/**", cors);
        return config;
    }

    @Bean
    public PasswordEncoder encoder() {
        return new BCryptPasswordEncoder(10);
    }
}