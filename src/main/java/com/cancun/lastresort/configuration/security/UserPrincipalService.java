package com.cancun.lastresort.configuration.security;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.cancun.lastresort.domain.entity.Privilege;
import com.cancun.lastresort.domain.entity.Roles;
import com.cancun.lastresort.domain.entity.User;
import com.cancun.lastresort.service.UserService;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class UserPrincipalService implements UserDetailsService {

    private final UserService userService;

    @Override
    public UserDetails loadUserByUsername(String email) {
        User user = userService.findByEmail(email);

        if (Objects.isNull(user)) {
            throw new UsernameNotFoundException(email);
        } else {
            return new UserPrincipal(user, getAuthorities(user.getRoles()));
        }
    }

    public List<GrantedAuthority> getAuthorities(Set<Roles> roles) {
        return getGrantedAuthorities(getPrivileges(roles));
    }

    public List<GrantedAuthority> getGrantedAuthorities(Set<String> privileges) {
        return privileges.stream().map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    }

    public Set<String> getPrivileges(Set<Roles> roles) {
        return roles.stream().flatMap(
            role -> role.getPrivileges().stream().map(Privilege::getName)).collect(Collectors.toSet());
    }
}
