package com.cancun.lastresort.domain.dto;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class LoginDTO {

    private Boolean success;
    private Boolean isAdmin;
    private Boolean isEnabled;
    private String message;
    private UserData userData;
    private String jwttoken;
}
