package com.cancun.lastresort.domain.dto;

import java.time.LocalDate;

import lombok.Data;

@Data
public class UserDTO {
    private Long id;
    private String email;
    private String username;
    private LocalDate birthday;
    private String phone;
}
