package com.cancun.lastresort.domain.dto;

import java.util.List;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class UserData {

    private final Long id;
    private final String email;
    private final String username;
    private final String birthday;
    private final List<String> privileges;
}
