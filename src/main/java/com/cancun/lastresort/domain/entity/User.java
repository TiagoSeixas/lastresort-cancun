package com.cancun.lastresort.domain.entity;

import java.time.LocalDate;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EntityListeners;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;

import org.hibernate.annotations.Type;

import com.cancun.lastresort.listener.UserListener;

import lombok.Data;

@Data
@Entity
@Table(name = "USER", schema = "LAST_RESORT")
@EntityListeners(UserListener.class)
public class User extends AuditModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull(message = "Please set a username to the User")
    @Type(type = "text")
    @Column(name = "username")
    private String username;

    @NotNull(message = "Please set an email to the User")
    @Type(type = "text")
    @Column(name = "email")
    private String email;

    @Type(type = "text")
    @Column(name = "phone")
    private String phone;

    @Column(name = "birthday")
    private LocalDate birthday;

    @NotNull(message = "Please set a password to the User")
    @Type(type = "text")
    @Column(name = "password")
    private String password;

    @Column(name = "enabled")
    private Boolean enabled = true;

    @ManyToMany(fetch = FetchType.EAGER, cascade = {CascadeType.PERSIST, CascadeType.MERGE})
    @JoinTable(
        name = "user_roles",
        joinColumns = {@JoinColumn(name = "user_id")},
        inverseJoinColumns = {@JoinColumn(name = "roles_id")})
    private Set<Roles> roles = new HashSet<>();

    @Override
    public String toString() {
        return " User [(" + id + ") - username: '" + username + "', email: '" + email + "']";
    }
}
