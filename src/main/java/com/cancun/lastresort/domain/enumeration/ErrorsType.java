package com.cancun.lastresort.domain.enumeration;

public enum ErrorsType {

    EMAIL_TAKEN("This email is already taken!");

    private final String message;

    ErrorsType(String message) {
        this.message = message;
    }

    public String getMessage() {
        return message;
    }
}



