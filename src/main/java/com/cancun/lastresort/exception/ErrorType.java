package com.cancun.lastresort.exception;

import static org.springframework.http.HttpStatus.EXPECTATION_FAILED;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_ACCEPTABLE;
import static org.springframework.http.HttpStatus.NOT_FOUND;

import java.util.stream.Stream;

import org.springframework.http.HttpStatus;

import lombok.AllArgsConstructor;
import lombok.Getter;

@Getter
@AllArgsConstructor
public enum ErrorType {

    INVALID_BOOKING_DATES(EXPECTATION_FAILED, "Booking dates informed are not available for booking!"),
    INVALID_BOOKING_MESSAGE(NOT_ACCEPTABLE, "BookingDate message is invalid!"),
    NO_AVAILABLE_BOOKING_DATES(NOT_FOUND, "There is any booking dates available!"),
    GENERIC_ERROR(INTERNAL_SERVER_ERROR, "An error has occurred when trying to communicate with server!"),
    SERVICE_UNAVAILABLE(HttpStatus.SERVICE_UNAVAILABLE, "The service requested is unavailable!");

    private final HttpStatus status;
    private final String message;

    public static ErrorType from(final HttpStatus status) {
        return Stream.of(ErrorType.values())
            .filter(er -> er.getStatus() == status)
            .findFirst()
            .orElse(GENERIC_ERROR);
    }
}



