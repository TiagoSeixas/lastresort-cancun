package com.cancun.lastresort.exception;

import static org.springframework.http.HttpStatus.SERVICE_UNAVAILABLE;
import static org.springframework.http.HttpStatus.valueOf;

import java.net.ConnectException;
import java.util.Map;

import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.AuthorizationServiceException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.google.common.collect.ImmutableMap;

import feign.FeignException;

@ControllerAdvice
public class ExceptionsHandler extends ResponseEntityExceptionHandler {

    private static final String EXCEPTION = "exception";

    @ExceptionHandler({AccessDeniedException.class})
    public ResponseEntity<Object> handleAccessDeniedException(final Exception ex, final WebRequest request) {
        return new
            ResponseEntity<>(
            ImmutableMap.of(
                EXCEPTION, createExceptionJson(HttpStatus.FORBIDDEN.value(), "Access denied!")),
            new HttpHeaders(), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler({AuthorizationServiceException.class})
    public ResponseEntity<Object> handleAuthorizationServiceException(final Exception ex, final WebRequest request) {
        return new
            ResponseEntity<>(
            ImmutableMap.of(
                EXCEPTION, createExceptionJson(HttpStatus.FORBIDDEN.value(), ex.getMessage())),
            new HttpHeaders(), HttpStatus.FORBIDDEN);
    }

    @ExceptionHandler({UserHasNoAuthorityException.class})
    public ResponseEntity<Object> handleUserHasNoAuthorityException(final Exception ex) {
        return new
            ResponseEntity<>(
            ImmutableMap.of(
                EXCEPTION, createExceptionJson(HttpStatus.UNAUTHORIZED.value(), ex.getLocalizedMessage())),
            new HttpHeaders(), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler({UserNotPermittedException.class})
    public ResponseEntity<Object> handleUserNotPermittedException(final Exception ex) {
        return new
            ResponseEntity<>(
            ImmutableMap.of(
                EXCEPTION, createExceptionJson(HttpStatus.NOT_ACCEPTABLE.value(), ex.getLocalizedMessage())),
            new HttpHeaders(), HttpStatus.NOT_ACCEPTABLE);
    }

    @ExceptionHandler(FeignException.class)
    public ResponseEntity<Object> handleFeignStatusException(final FeignException ex, final WebRequest request) {
        return new
            ResponseEntity<>(
            ImmutableMap.of(
                EXCEPTION, createExceptionJson(ex.status(), captureFeignMessage(ex))),
            new HttpHeaders(), valueOf(ex.status()));
    }

    @ExceptionHandler(ConnectException.class)
    public ResponseEntity<Object> handleConnectException(final ConnectException ex, final WebRequest request) {
        return new
            ResponseEntity<>(
            ImmutableMap.of(
                EXCEPTION, createExceptionJson(SERVICE_UNAVAILABLE.value(), "The booking service is unavailable!")),
            new HttpHeaders(), SERVICE_UNAVAILABLE);
    }

    private String captureFeignMessage(final FeignException ex) {
        final String message = ex.getLocalizedMessage();
        if (ex.status() == 404) {
            return String.format("Service requested: '%s', was not found!", ex.request().url());
        } else if (ex.status() >= 400 && ex.status() < 500) {
            return message.substring(message.lastIndexOf("message\":\"") + 10, message.length() - 4);
        } else if (ex.status() == 503) {
            return "The service you're trying to access is unavailable!";
        }
        return "An error has occurred when trying to access the booking services!";
    }

    public Map<String, Object> createExceptionJson(final Integer httpStatus, final String message) {
        return ImmutableMap.of(
            "httpStatus", httpStatus,
            "message", message);
    }

}
