package com.cancun.lastresort.exception;

public class UserHasNoAuthorityException extends RuntimeException {

    private static final long serialVersionUID = 2307568593080441909L;

    public UserHasNoAuthorityException() {
        super();
    }
}
