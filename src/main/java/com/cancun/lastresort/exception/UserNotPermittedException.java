package com.cancun.lastresort.exception;

public class UserNotPermittedException extends RuntimeException {
    private static final long serialVersionUID = 1L;

    public UserNotPermittedException(String message) {
        super(message);
    }
}
