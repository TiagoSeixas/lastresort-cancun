package com.cancun.lastresort.integration;

import java.util.List;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;

import com.cancun.lastresort.domain.dto.AvailabilityDTO;
import com.cancun.lastresort.domain.dto.BookingDTO;
import com.cancun.lastresort.domain.dto.PageDTO;
import com.cancun.lastresort.domain.entity.Booking;

@FeignClient("lastresort-availability-api")
public interface LastResortAvalabilityClient {

    @RequestMapping("/get-available-days")
    PageDTO<AvailabilityDTO> getAvailableDays();

    @GetMapping("/get-bookings")
    List<BookingDTO> getBookings();

    @PostMapping("/book-dates/{userId}")
    PageDTO<AvailabilityDTO> bookDates(@PathVariable final Long userId, @RequestBody final List<Long> dateIds);

    @PutMapping("/modify-booking/{bookingId}")
    void modifyBooking(@RequestBody final Booking booking, @PathVariable final Long bookingId);

    @DeleteMapping("/cancel-booking/{bookingId}")
    void cancelBooking(@PathVariable final Long bookingId);
}