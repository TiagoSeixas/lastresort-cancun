package com.cancun.lastresort.listener;

import static com.cancun.lastresort.configuration.AutowireHelper.autowire;
import static com.cancun.lastresort.domain.enumeration.ErrorsType.EMAIL_TAKEN;
import static org.apache.commons.lang.BooleanUtils.isTrue;

import java.util.Set;

import javax.persistence.PostPersist;
import javax.persistence.PrePersist;

import org.apache.commons.lang.BooleanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;

import com.cancun.lastresort.domain.entity.User;
import com.cancun.lastresort.exception.UserNotPermittedException;
import com.cancun.lastresort.service.RoleService;
import com.cancun.lastresort.service.UserService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
public class UserListener {

    @Autowired
    private RoleService roleService;

    @Autowired
    private UserService userService;

    @Autowired
    private PasswordEncoder encoder;

    @PrePersist
    public void onPrePersist(User user) throws UserNotPermittedException {
        autowire(this, this.userService);
        autowire(this, this.roleService);

        if (userService.existsByEmail(user.getEmail())) {
            throw new UserNotPermittedException(EMAIL_TAKEN.getMessage());
        }

        user.setPassword(encoder.encode(user.getPassword()));

        if (isTrue(user.getRoles().isEmpty()))
        user.setRoles(Set.of(roleService.getUserRole()));
    }

    @PostPersist
    public void onPostPersist(User user) {}
}
