package com.cancun.lastresort.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.cancun.lastresort.domain.entity.Roles;

@Repository
public interface RoleRepository extends JpaRepository<Roles, Long> {

    Roles findByName(String name);

    boolean existsByName(String name);
}
