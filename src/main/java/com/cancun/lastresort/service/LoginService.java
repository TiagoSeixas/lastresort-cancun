package com.cancun.lastresort.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.cancun.lastresort.domain.dto.LoginDTO;
import com.cancun.lastresort.domain.dto.UserData;
import com.cancun.lastresort.domain.dto.UserData.UserDataBuilder;
import com.cancun.lastresort.domain.entity.User;
import com.cancun.lastresort.exception.UserHasNoAuthorityException;
import com.cancun.lastresort.configuration.security.JwtTokenUtil;
import com.cancun.lastresort.configuration.security.UserPrincipal;
import com.cancun.lastresort.configuration.security.UserPrincipalService;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
@RequiredArgsConstructor
public class LoginService {

    private static final String USERNAME_NOT_FOUND = "The username is incorrect!";
    private static final String BAD_CREDENTIAL = "The password is incorrect!";
    private static final String NO_ACCESS = "This user has any access to the LastResorts services!";

    private final AuthenticationManager authManager;
    private final JwtTokenUtil jwtTokenUtil;
    private final UserPrincipalService userPrincipalService;
    private final RoleService roleService;

    public LoginDTO login(User user) {
        LoginDTO loginDTO = new LoginDTO();
        try {
            UserPrincipal userPrincipal = (UserPrincipal) userPrincipalService.loadUserByUsername(user.getEmail());
            UsernamePasswordAuthenticationToken authenticationToken = new UsernamePasswordAuthenticationToken(
                userPrincipal, user.getPassword(), userPrincipal.getAuthorities());
            authManager.authenticate(authenticationToken).isAuthenticated();
            loginDTO = createLoginDTO(userPrincipal);

            String jwttoken = jwtTokenUtil.generateToken(userPrincipal);
            loginDTO.setJwttoken(jwttoken);

        } catch (UsernameNotFoundException e) {
            loginDTO.setSuccess(Boolean.FALSE);
            loginDTO.setMessage(USERNAME_NOT_FOUND);
            log.warn(USERNAME_NOT_FOUND);

        } catch (BadCredentialsException e) {
            loginDTO.setSuccess(Boolean.FALSE);
            loginDTO.setMessage(BAD_CREDENTIAL);
            log.warn(BAD_CREDENTIAL);

        } catch (UserHasNoAuthorityException e) {
            loginDTO.setSuccess(Boolean.FALSE);
            loginDTO.setMessage(NO_ACCESS);
            log.warn(NO_ACCESS);
        }
        return loginDTO;
    }

    public LoginDTO createLoginDTO(UserPrincipal userPrincipal) {
        LoginDTO loginDTO = new LoginDTO();
        loginDTO.setUserData(createUserData(userPrincipal));
        loginDTO.setIsAdmin(userPrincipal.getApplicationUser().getRoles().contains(roleService.getAdminRole()));
        loginDTO.setIsEnabled(userPrincipal.isEnabled());
        loginDTO.setSuccess(Boolean.TRUE);
        return loginDTO;
    }

    private UserData createUserData(UserPrincipal userPrincipal) {
        User user = userPrincipal.getApplicationUser();

        UserDataBuilder userDataBuilder = UserData.builder()
            .id(user.getId())
            .email(user.getEmail())
            .username(user.getUsername());

        List<String> authorities = userPrincipal.getAuthorities()
            .stream()
            .map(GrantedAuthority::getAuthority)
            .collect(Collectors.toList());

        if (authorities.isEmpty()) {
            throw new UserHasNoAuthorityException();
        }
        return userDataBuilder.privileges(authorities).build();
    }
}
