package com.cancun.lastresort.service;

import org.springframework.stereotype.Service;

import com.cancun.lastresort.domain.entity.Roles;
import com.cancun.lastresort.repository.RoleRepository;

import lombok.RequiredArgsConstructor;

@Service
@RequiredArgsConstructor
public class RoleService {

    private final RoleRepository roleRepository;

    public Roles getAdminRole() {
        return roleRepository.findByName("ROLE_ADMIN");
    }

    public Roles getHotelAdminRole() {
        return roleRepository.findByName("HOTEL_MANAGER");
    }

    public Roles getUserRole() {
        return roleRepository.findByName("ROLE_USER");
    }

}
