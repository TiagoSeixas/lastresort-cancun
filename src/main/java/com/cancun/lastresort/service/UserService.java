package com.cancun.lastresort.service;

import org.springframework.stereotype.Service;

import com.cancun.lastresort.domain.dto.UserDTO;
import com.cancun.lastresort.domain.entity.User;
import com.cancun.lastresort.repository.UserRepository;

import lombok.RequiredArgsConstructor;
import ma.glasnost.orika.MapperFacade;

@Service
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;
    private final MapperFacade mapper;

    public User findByEmail(final String email) {
        return userRepository.findByEmail(email);
    }

    public boolean existsByEmail(final String email) {
        return userRepository.existsByEmail(email);
    }

    public UserDTO createUser(final User user) {
        return mapper.map(userRepository.save(user), UserDTO.class);
    }
}