package com.cancun.lastresort.util;

import static com.cancun.lastresort.domain.enumeration.PrivilegeEnum.PRIVILEGE_ADMIN_READ;
import static com.cancun.lastresort.domain.enumeration.PrivilegeEnum.PRIVILEGE_ADMIN_WRITE;
import static com.cancun.lastresort.domain.enumeration.PrivilegeEnum.PRIVILEGE_HOTEL_ADMIN_WRITE;
import static com.cancun.lastresort.domain.enumeration.PrivilegeEnum.PRIVILEGE_USER_READ;
import static com.cancun.lastresort.domain.enumeration.PrivilegeEnum.PRIVILEGE_USER_WRITE;
import static java.util.Arrays.asList;

import java.util.List;

import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

import com.cancun.lastresort.domain.entity.Privilege;
import com.cancun.lastresort.domain.entity.Roles;
import com.cancun.lastresort.domain.entity.User;
import com.cancun.lastresort.repository.PrivilegeRepository;
import com.cancun.lastresort.repository.RoleRepository;
import com.cancun.lastresort.repository.UserRepository;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Component
@RequiredArgsConstructor
public class InitialDataLoader implements ApplicationListener<ContextRefreshedEvent> {

    public static final String DONE = ">-- Done!";
    private final PrivilegeRepository privilegeRepository;
    private final RoleRepository roleRepository;
    private final UserRepository userRepository;

    @Override
    public void onApplicationEvent(final ContextRefreshedEvent event) {
        log.info("> Inserting Privileges...");
        final Privilege privilegeAdminRead = createPrivilegeIfNotFound(PRIVILEGE_ADMIN_READ.name());
        final Privilege privilegeAdminWrite = createPrivilegeIfNotFound(PRIVILEGE_ADMIN_WRITE.name());
        final Privilege privilegeHotelAdminRead = createPrivilegeIfNotFound(PRIVILEGE_HOTEL_ADMIN_WRITE.name());
        final Privilege privilegeHotelAdminWrite = createPrivilegeIfNotFound(PRIVILEGE_HOTEL_ADMIN_WRITE.name());
        final Privilege privilegeUserRead = createPrivilegeIfNotFound(PRIVILEGE_USER_READ.name());
        final Privilege privilegeUserWrite = createPrivilegeIfNotFound(PRIVILEGE_USER_WRITE.name());
        log.info(DONE);

        log.info("> Inserting Roles...");
        createRoleIfNotFound("ROLE_ADMIN", asList(privilegeAdminRead, privilegeAdminWrite));
        createRoleIfNotFound("HOTEL_MANAGER", asList(privilegeHotelAdminRead, privilegeHotelAdminWrite));
        createRoleIfNotFound("ROLE_USER", asList(privilegeUserRead, privilegeUserWrite));
        log.info(DONE);

        log.info("> Inserting Users...");
        createHotelManagerIfNotFound("hotelmanager");
        createSystemAdminIfNotFound("admin");
        log.info(DONE);
    }

    protected Privilege createPrivilegeIfNotFound(final String name) {
        if (!privilegeRepository.existsByName(name)) {
            final Privilege privilege = new Privilege();
            privilege.setName(name);
            return privilegeRepository.save(privilege);
        }
        return privilegeRepository.findByName(name);
    }

    protected Roles createRoleIfNotFound(final String name, final List<Privilege> privileges) {
        if (!roleRepository.existsByName(name)) {
            final Roles role = new Roles();
            role.setName(name);
            role.setPrivileges(privileges);
            return roleRepository.save(role);
        }
        return roleRepository.findByName(name);
    }

    protected User createHotelManagerIfNotFound(final String email) {
        if (!userRepository.existsByEmail(email + "@gmail.com")) {
            final User user = new User();
            user.setUsername(email);
            user.setEmail(email + "@gmail.com");
            user.setPassword(email);
            return userRepository.save(user);
        }
        return userRepository.findByEmail(email);
    }

    protected User createSystemAdminIfNotFound(final String email) {
        if (!userRepository.existsByEmail(email + "@gmail.com")) {
            final User user = new User();
            user.setUsername(email);
            user.setEmail(email + "@gmail.com");
            user.setPassword(email);
            return userRepository.save(user);
        }
        return userRepository.findByEmail(email);
    }
}
