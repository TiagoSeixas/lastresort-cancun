package com.cancun.lastresort.web;

import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cancun.lastresort.domain.dto.AvailabilityDTO;
import com.cancun.lastresort.domain.dto.BookingDTO;
import com.cancun.lastresort.domain.dto.PageDTO;
import com.cancun.lastresort.domain.entity.Booking;
import com.cancun.lastresort.integration.LastResortAvalabilityClient;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/availability")
public class AvalabilityController {

    private final LastResortAvalabilityClient availabilityClient;

    @CrossOrigin
    @GetMapping("/get-available-days")
    public PageDTO<AvailabilityDTO> getAvailableDays() {
        return availabilityClient.getAvailableDays();
    }

    @CrossOrigin
    @GetMapping("/get-bookings")
    public List<BookingDTO> getBookings() {
        return availabilityClient.getBookings();
    }

    @CrossOrigin
    @PostMapping("/book-dates/{userId}")
    public void bookDates(@PathVariable final Long userId, @RequestBody final List<Long> dateIds) {
        availabilityClient.bookDates(userId, dateIds);
    }

    @CrossOrigin
    @PutMapping("/modify-booking/{bookingId}")
    public void modifyBooking(@RequestBody final Booking booking, @PathVariable final Long bookingId) {
        availabilityClient.modifyBooking(booking, bookingId);
    }

    @CrossOrigin
    @DeleteMapping("/cancel-booking/{bookingId}")
    public void cancelBooking(@PathVariable final Long bookingId) {
        availabilityClient.cancelBooking(bookingId);
    }
}
