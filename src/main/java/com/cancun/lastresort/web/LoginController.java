package com.cancun.lastresort.web;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.cancun.lastresort.domain.dto.LoginDTO;
import com.cancun.lastresort.domain.dto.UserDTO;
import com.cancun.lastresort.domain.entity.User;
import com.cancun.lastresort.service.LoginService;
import com.cancun.lastresort.service.UserService;

import lombok.RequiredArgsConstructor;

@RestController
@RequiredArgsConstructor
@RequestMapping("/auth")
public class LoginController {

    private final LoginService loginService;
    private final UserService userService;

    @CrossOrigin
    @PostMapping("/signin")
    public LoginDTO login(@RequestBody User user) {
        return loginService.login(user);
    }

    @CrossOrigin
    @PostMapping("/register")
    public UserDTO createUser(@RequestBody User user) {
        return userService.createUser(user);
    }
}
